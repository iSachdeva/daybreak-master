//
//  Choice.swift
//  DayBreak
//
//  Created by Amit on 20/06/2016.
//  Copyright © 2016 Amit. All rights reserved.
//

import Foundation
import ObjectMapper

class ChoiceResponse: Mappable {
    
    var recommended:[Choice]?
    var myChoices:[Choice]?
    
    required init?(_ map: Map){
        
    }
    
    func mapping(map: Map) {
        
        recommended <- map["recommended"]
        myChoices <- map["mychoices"]
    }
}

class Choice:Mappable {
    
    var id:String?
    var title:String?
    var subtitle:String?
    var blurb:String?
    var instructions:String?
    var time_to_complete:String?
    var cts:String?
    var cta_action:String?
    var feedback:String?

    required init?(_ map: Map){
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        title <- map["title"]
        subtitle <- map["subtitle"]
        blurb <- map["blurb"]
        instructions <- map["instructions"]
        time_to_complete <- map["time_to_complete"]
        cts <- map["cts"]
        cta_action <- map["cta_action"]
        feedback <- map["feedback"]
    }
    
}



