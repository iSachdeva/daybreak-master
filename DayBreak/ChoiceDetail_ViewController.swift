//
//  ChoiceDetail_ViewController.swift
//  DayBreak
//
//  Created by Amit on 21/06/2016.
//  Copyright © 2016 Amit. All rights reserved.
//

import Foundation
import UIKit

class ChoiceDetail_ViewController: UIViewController {

    @IBOutlet var subTitleLabel:UILabel!
    @IBOutlet var instructionsLabel:UILabel!
    @IBOutlet var notForMeButton:UIButton!
    @IBOutlet var chooseToDoButton:UIButton!
    
    var aRecommendedChoice:Choice?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadData()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func loadData() {
        
        if let recommendedChoice = self.aRecommendedChoice {
            
            if let title = recommendedChoice.title {
                self.navigationItem.title = title
            } else {
                self.navigationItem.title = ""
            }
            
            if let subTitle = recommendedChoice.subtitle {
                self.subTitleLabel.text = subTitle
            } else {
                self.subTitleLabel.text = ""
            }
            
            if let instrunctions = recommendedChoice.instructions {
                self.instructionsLabel.text = instrunctions
            } else {
            }
        } else {
            self.subTitleLabel.text = ""
            self.instructionsLabel.text = ""
            self.chooseToDoButton.hidden = true
            self.notForMeButton.hidden = true
        }
    }
    
    @IBAction func notForMeClicked(sender:UIButton?) {
    NSNotificationCenter.defaultCenter().postNotificationName("ChoiceStatusDidChangeToNotForMeNotification", object: self.aRecommendedChoice, userInfo: nil)
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func choosenToDoClicked(sender:UIButton?) {
        NSNotificationCenter.defaultCenter().postNotificationName("ChoiceStatusDidChangeToChooseToDoNotification", object: self.aRecommendedChoice, userInfo: nil)
        self.navigationController?.popViewControllerAnimated(true)
    }
    
}
