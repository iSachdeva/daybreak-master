//
//  ChoiceEngine.swift
//  DayBreak
//
//  Created by Amit on 20/06/2016.
//  Copyright © 2016 Amit. All rights reserved.
//

import Foundation
import UIKit

enum ChoiceEngineRequestType {
    
    case  getChoiceList
}

protocol ChoiceEngine_Delegate {
    
    func didRequestFinishedWithSuccessResponse(response:AnyObject, requestType:ChoiceEngineRequestType)
    func didRequestFailedWithErrorResponse(errorResponse:NSError, requestType:ChoiceEngineRequestType)
}

class ChoiceEngine: NSObject,NetworkConnection_Delegate {
    
    var delegate:ChoiceEngine_Delegate?
    
    //MARK:- init()
    init(delegate_:ChoiceEngine_Delegate) {
        super.init()
        self.delegate = delegate_
    }
    
    func getChoiceList() {
       
        let parameters = NSMutableDictionary()
        //can add required parameters
    
        NetworkConnection.init(delegate_: self).networkRequestToGetChoices(parameters)
    }
    
    //MARK:- NetworkConnection Delegate
    func networkConnectionDidSuccessWithResponse(response: AnyObject?, requestType: NetworkRequestType) {
        
        switch requestType {
        case .getChoiceList:
            
            if let validResponse = response{
                self.delegate?.didRequestFinishedWithSuccessResponse(validResponse, requestType: .getChoiceList)
            } else {

            }
            break
        }
    }
    
    func networkConnectionDidFailedWithErrorResponse(response: NSError?, requestType: NetworkRequestType) {
        
        switch requestType {
        case .getChoiceList:
            self.delegate?.didRequestFailedWithErrorResponse(response!,requestType: .getChoiceList)

            break
        }
    }
    
}