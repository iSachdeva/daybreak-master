//
//  Choice_ViewController.swift
//  DayBreak
//
//  Created by Amit on 19/06/2016.
//  Copyright © 2016 Amit. All rights reserved.
//

import UIKit
import Foundation

class Choice_ViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, ChoiceEngine_Delegate{
    
    @IBOutlet var choiceTableView:UITableView!
    @IBOutlet var choiceSegmentControl:UISegmentedControl!
    
    var choiceEngine: ChoiceEngine!
    
    var recommendedChoices = [Choice]()
    var myChoices = [Choice]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        choiceEngine  = ChoiceEngine(delegate_: self)
        
        self.loadChoiceListData()
        self.registerForNotifications()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func loadChoiceListData() {
        //Start show progress indicator while loading of data from server
        //Call method to load data from server
        //same method can get call while refresh the list or pull to refresh the data
        //page by page also can implement by passing a parameter of load page count
        self.choiceEngine.getChoiceList()
    }
    
    func registerForNotifications() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(Choice_ViewController.notForMeSelected(_:)), name: "ChoiceStatusDidChangeToNotForMeNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(Choice_ViewController.chooseToDoSelected(_:)), name: "ChoiceStatusDidChangeToChooseToDoNotification", object: nil)
    }
    
    func notForMeSelected(notification:NSNotification) {
        
        let selectedChoice = notification.object as? Choice
        let index = self.recommendedChoices.indexOf({$0.id == selectedChoice?.id})
        self.recommendedChoices.removeAtIndex(index!)
        self.choiceTableView.reloadData()
    }
    
    func chooseToDoSelected(notification:NSNotification) {
        let selectedChoice = notification.object as? Choice
        let index = self.recommendedChoices.indexOf({$0.id == selectedChoice?.id})
        self.myChoices.append(self.recommendedChoices[index!])
        self.recommendedChoices.removeAtIndex(index!)
        self.choiceTableView.reloadData()
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    //MARK:- UITableView Delegate Methods
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if choiceSegmentControl.selectedSegmentIndex == 0 && self.myChoices.count > 0 {
            return self.myChoices.count
        } else if choiceSegmentControl.selectedSegmentIndex == 1 && self.recommendedChoices.count > 0 {
            return self.recommendedChoices.count
        } else {
            return 1
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if choiceSegmentControl.selectedSegmentIndex == 0 && self.myChoices.count > 0 {
            return 230
        } else if choiceSegmentControl.selectedSegmentIndex == 1 && self.recommendedChoices.count > 0 {
            return 90
        } else {
            return self.choiceTableView.bounds.height;
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        switch choiceSegmentControl.selectedSegmentIndex {
            
        case 0:
            if self.myChoices.count > 0 {
                let cell = tableView.dequeueReusableCellWithIdentifier("ChoiceCell_Identifier", forIndexPath: indexPath) as! MyChoices_TableViewCell
                
                cell.loadMyChoiceCellViewWithDetails(self.myChoices[indexPath.row])
                return cell
                
            } else {
                let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("EmptyRecommendedCell_Identifier", forIndexPath: indexPath)
                cell.selectionStyle = .None
                return cell
            }
            
        case 1:
            if self.recommendedChoices.count > 0 {
                let cell = tableView.dequeueReusableCellWithIdentifier("RecommendedCell_Identifier", forIndexPath: indexPath) as! Recommeded_TableViewCell
                
                cell.loadRecommendedCellViewWithDetails(self.recommendedChoices[indexPath.row])
                return cell
                
            } else {
                let  cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("EmptyChoicesCell_Identifier", forIndexPath: indexPath)
                cell.selectionStyle = .None

                return cell
            }
            
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if choiceSegmentControl.selectedSegmentIndex == 0 && self.myChoices.count > 0 {
            
        } else if choiceSegmentControl.selectedSegmentIndex == 1 && self.recommendedChoices.count > 0 {
            self.performSegueWithIdentifier("ChoiceListToRecommendedChoiceDetail_Id", sender: self.recommendedChoices[indexPath.row])
        } else {
            
        }
    }
    
    //MARK:- IBActions
    @IBAction func segmentControlIndexChanged(sender:AnyObject) {
        
        choiceTableView.reloadData()
    }
    
    //MARK:- Navgation delegate
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ChoiceListToRecommendedChoiceDetail_Id" {
            let choiceDetailViewController = segue.destinationViewController as! ChoiceDetail_ViewController
            choiceDetailViewController.aRecommendedChoice = sender as? Choice
        } else {
            
        }
    }
    
    //MARK:- Account Engine Delegate methods
    func didRequestFinishedWithSuccessResponse(response:AnyObject, requestType:ChoiceEngineRequestType) {
        
        switch requestType {
        case .getChoiceList:
            
            //Dismiss the loading indicator
            
            //remove the previous items if any
            if self.recommendedChoices.count > 1 {
                self.recommendedChoices.removeAll()
            }
            if self.myChoices.count > 1 {
                self.myChoices.removeAll()
            }
            
            //insert new data into array list
            let choiceList = response as! ChoiceResponse
            if let recommendedChoiceResponse = choiceList.recommended {
                recommendedChoices.appendContentsOf(recommendedChoiceResponse)
            }
            if let myChoiceResponse = choiceList.myChoices {
                myChoices.appendContentsOf(myChoiceResponse)
            }
            
            //reload the list view with latest data
            self.choiceTableView.reloadData()
            
            break
        }
    }
    
    func didRequestFailedWithErrorResponse(errorResponse:NSError, requestType:ChoiceEngineRequestType) {
        
        //Show error alert messages
    }
    
}
