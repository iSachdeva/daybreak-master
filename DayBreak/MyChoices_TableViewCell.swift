//
//  MyChoices_TableViewCell.swift
//  DayBreak
//
//  Created by Amit on 21/06/2016.
//  Copyright © 2016 Amit. All rights reserved.
//

import UIKit
import Foundation


class MyChoices_TableViewCell: UITableViewCell {

    @IBOutlet var choiceTitleLabel:UILabel!
    @IBOutlet var choiceSubTitleLabel:UILabel!
    @IBOutlet var blurbLabel:UILabel!
    
    func loadMyChoiceCellViewWithDetails(myChoice:Choice) {
        
        self.selectionStyle = .None
        
        if let title = myChoice.subtitle {
            self.choiceTitleLabel.text = title
        }
        if let subTitle = myChoice.subtitle {
            self.choiceSubTitleLabel.text = subTitle
        }
        
        if let blurb = myChoice.blurb {
            self.blurbLabel.text = blurb
        }
        
        //  timeLabel.text = ""   And So on
    }
    
}
