//
//  NetworkConnection.swift
//  DayBreak
//
//  Created by Amit on 20/06/2016.
//  Copyright © 2016 Amit. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

enum NetworkRequestType {
    
    case  getChoiceList
}

protocol NetworkConnection_Delegate {
    
    func networkConnectionDidSuccessWithResponse(response:AnyObject?,requestType:NetworkRequestType)
    func networkConnectionDidFailedWithErrorResponse(response:NSError?,requestType:NetworkRequestType)
}

class NetworkConnection: NSObject {
    
    var delegate:NetworkConnection_Delegate?
    
    init(delegate_:NetworkConnection_Delegate) {
        super.init()
        self.delegate = delegate_
    }
    
    func networkRequestToGetChoices(postParameters:NSDictionary) {
        
        //Here we can create network request by the help NSURLConnection class OR we can uses Almofire library (third party) to make the request
        //At this moment, we will read the data from local file which contains the Json response data
        
        let url = NSBundle.mainBundle().URLForResource("Daybreak", withExtension: "json")
        let data = NSData(contentsOfURL: url!)
        
        do {
            let choiceJsonResponse = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
            let result = Mapper<ChoiceResponse>().map(choiceJsonResponse)
            self.delegate!.networkConnectionDidSuccessWithResponse(result, requestType: .getChoiceList)
            
        } catch {
            // Handle Error
            print(error)
            self.delegate!.networkConnectionDidFailedWithErrorResponse(nil, requestType: .getChoiceList)
        }
    }
}