//
//  Recommeded_TableViewCell.swift
//  DayBreak
//
//  Created by Amit on 21/06/2016.
//  Copyright © 2016 Amit. All rights reserved.
//

import UIKit
import Foundation

class Recommeded_TableViewCell: UITableViewCell {

    @IBOutlet var choiceTitleLabel:UILabel!
    @IBOutlet var blurbLabel:UILabel!
    @IBOutlet var timeLabel:UILabel!
    
    
    func loadRecommendedCellViewWithDetails(recommendedChoice:Choice) {
        
        self.selectionStyle = .Gray

        if let subTitle = recommendedChoice.subtitle {
            self.choiceTitleLabel.text = subTitle
        }
        
        if let blurb = recommendedChoice.blurb {
            self.blurbLabel.text = blurb
        }
        
      //  timeLabel.text = ""   And So on
    }
    
}
