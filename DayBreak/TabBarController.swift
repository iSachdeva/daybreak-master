//
//  TabBarController.swift
//  DayBreak
//
//  Created by Amit on 19/06/2016.
//  Copyright © 2016 Amit. All rights reserved.
//

import UIKit
import Foundation

class TabBarController: UITabBarController {
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let view = UIButton.init(frame: CGRectMake(self.view.bounds.maxX/2 - 30, self.view.bounds.maxY - 60, 55, 55))
        view.backgroundColor = UIColor.clearColor()
        view.setImage(UIImage.init(imageLiteral: "plus"), forState: .Normal)
        self.view.addSubview(view)
    }

}
